const express = require('express');
const app = express();

const route = require('./route');

app.use('/api',route);

app.listen(3000,() => {
    console.log(`server is running on 3000`);
})