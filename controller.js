const axios = require('axios');
const logger = require('./logger');

exports.getData = async(req,res)=>{
    try{
        const {eventid} = req.query
        const url = 'https://stocknewsapi.com/api/v1/events?&token=1pufcgel8ixjplzvrv5nyyiuxkwipidp2ze1vxhx';
        let response;

        if(eventid){
            var news = [];
            response = await axios.get(`${url}&eventid=${eventid}`);
            logger.info(`eventid pass:${eventid}`);
            //  res.send({data:response.data.data})
            // console.log("Length:"+response.data.data.length)
            for(i=0;i<response.data.data.length;i++){
                var newsUrl = response.data.data[i].news_url;
                var imageUrl = response.data.data[i].image_url;
                var sourceName = response.data.data[i].source_name;
                var date = response.data.data[i].date;

                news.push({newsurl:newsUrl,image_url:imageUrl,source_name:sourceName,date:date})
                // console.log({newsurl:newsUrl,image_url:imageUrl,source_name:sourceName,date:date} 
            }
            return res.json(news)   
        }else{
            response = await axios.get(url);
            logger.info('params not passed')
           return res.send({data:response.data.data})
        }    
    }
    catch(err){
        res.send(err.message)
    }
}